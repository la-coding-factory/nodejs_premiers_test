/* Version ES5 */
/* On importe 2 fois lib2, une fois "complète" */
const lib2 = require('./lib2')
/* Ici on importe uniquement l'export nommé */
const { named_export } = require('./lib2')

console.log("Bonjour et bienvenue dans APP 2");

console.log(lib2.named_export(1))
console.log(lib2.named_export(2))
console.log(named_export(4))
