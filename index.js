/* Pour recuperer les arguments de la ligne de commande */
console.log(process.argv)

/* les differentes variables */
const a = 5 // constante
var b = 6 // variable (ancienne)
let c = 7 // variable (nouvelle)

/* conditions */
if (a == b) {
	console.log('a == b')
}

/* les boucles */
for (let i = 0; i < 10; i++) {
	console.log('i = ' + i);
}
let x = 0
while (x < 5) {
	console.log(++x + ' == x');
}

/* accéder à un tableau */
const tab = [0, 1, 2, a, b, c]
console.log(tab[3])

console.log(obj.a, obj.coucou, obj.color, obj.obj_f(4))


/* Le scope (la portée) des variables et les closures */
function test_f(a) {
	const b = a * a;

	return function(c, d) {
		return c + d + b
	}
}
// a quoi correspond "question_one" ?
const question_one = test_f(2)
const question_two = question_one(5, 9)

/* Les objets */
const obj = {
	a: 2,
	coucou: 'Hello World',
	color: 'red',
	obj_f: function(d) { return d * 2; }
}

/* La destructuration */
const { a, coucou, color } = obj
console.log(a, coucou, color)


