/* Ces 2 lignes ne s'executeront qu'une seule fois, maglré le fait que l'on require 2 fois le fichier */
console.log('Lib 2 is executed')
const base_number = 100

/* Export nommé "named_export" contenant la function "une_fonction" */
module.exports.named_export = function une_fonction(a) {
	return (a * 2) + base_number;
}
